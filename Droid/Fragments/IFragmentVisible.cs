﻿namespace EntregaXamarin1.Droid
{
    interface IFragmentVisible
    {
        void BecameVisible();
    }
}
