﻿using System;
using Android.Views;

namespace EntregaXamarin1.Droid
{
    public class RecyclerClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}
