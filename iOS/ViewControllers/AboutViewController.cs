﻿using System;
using UIKit;

namespace EntregaXamarin1.iOS
{
    public partial class AboutViewController : UIViewController
    {
        public AboutViewModel ViewModel { get; set; }
        public AboutViewController(IntPtr handle) : base(handle)
        {
            ViewModel = new AboutViewModel();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = ViewModel.Title;

            AppNameLabel.Text = "Xamarin with Realm";
            VersionLabel.Text = "Lluís Criado";
            AboutTextView.Text = "This app is written in C# and native APIs using the Xamarin Platform. It uses Realm as local database.";
        }
    }
}
