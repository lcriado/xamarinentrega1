﻿using System;
using UIKit;

namespace EntregaXamarin1.iOS
{
    public partial class TabBarController : UITabBarController
    {
        public TabBarController(IntPtr handle) : base(handle)
        {
            TabBar.Items[0].Title = "Argentina";
            TabBar.Items[1].Title = "About";
        }
    }
}
